from django.shortcuts import render

def index(request):
    return render(request, "index.html")

def contact(request):
    return render(request, "CONTACT.html")

def gallery(request):
    return render(request, "GALLERY.html")

def about(request):
    return render(request, "ABOUT.html")
