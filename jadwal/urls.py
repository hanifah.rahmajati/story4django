"""myweb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views
from .views import schedule, schedule_create, schedule_delete, schedule_selectdelete
from story4 import views

urlpatterns = [
    path('schedule/', schedule, name='schedule'),
    path('schedule/create', schedule_create, name='schedule_create'),
    path( 'schedule/delete', schedule_delete, name='schedule_delete'),
    path( 'schedule/selectdelete/<int:id>/', schedule_selectdelete, name='schedule_selectdelete'),
    path('', views.index, name="index"),
    path('ABOUT', views.about, name="about"),
    path('CONTACT', views.about, name="contact"),
    path('GALLERY', views.about, name="gallery"),
]
