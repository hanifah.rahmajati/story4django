from django.db import models
from datetime import datetime
# Create your models here.
class Schedule(models.Model):
    date = models.DateTimeField(default=datetime.now)
    time = models.TimeField()
    category = models.CharField(max_length=20)
    name = models.CharField(max_length=20)
    place = models.CharField(max_length=20)

    def __str__(self):
        return self.date

    


# Create your models here.
