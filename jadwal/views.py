from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Schedule
from . import forms

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'hasil.html', {'schedules': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('schedule')

    else:
        form = forms.ScheduleForm()
    return render(request, 'jadwalku.html', {'form': form})

def schedule_delete(request):
    Schedule.objects.all().delete()
    return render(request, 'hasil.html')

def schedule_selectdelete(request, id):
    if request.method == "POST":
        Schedule.objects.filter(id=id).delete()
        return redirect('schedule')
    else:
        return HttpResponse('/Get not allowed')
